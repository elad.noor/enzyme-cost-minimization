Deprecated
==========

This repository is deprecated and will not be updated after January 27th, 2020.

All functionality has been ported to [`equilibrator-pathway`](https://gitlab.com/equilibrator/equilibrator-pathway)

Therefore, we strongly advise against using this repository.
