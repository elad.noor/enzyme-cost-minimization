import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

from ecm.cost_function import EnzymeCostFunction
from ecm.simulator import EnzymeCostSimulator


if not os.path.exists("res"):
    os.mkdir("res")

Nc = 3
Nr = 3

S = np.zeros((Nc, Nr))
v = np.zeros((Nr, 1))  # [umol/min]
kcat = np.zeros((Nr, 1))  # [umol/min/mg]
dG0 = np.zeros((Nr, 1))  # [kJ/mol]
KMM = np.ones((Nc, Nr))  # [M]
A_act = np.zeros((Nc, Nr))
K_act = np.ones((Nc, Nr))  # [M]
A_inh = np.zeros((Nc, Nr))
K_inh = np.ones((Nc, Nr))  # [M]

# v0: X0 -> X1
S[0, 0] = -1.0
S[1, 0] = 1.0
v[0] = 2.0
kcat[0] = 20.0
dG0[0] = -30
KMM[0, 0] = 1e-2
KMM[1, 0] = 1e-4

# v1: X1 -> X2
S[1, 1] = -1.0
S[2, 1] = 1.0
v[1] = 1.0
kcat[1] = 8.0
dG0[1] = -20
KMM[1, 1] = 1e-4
KMM[2, 1] = 1e-1

# v2: X1 -> X2
S[1, 2] = -1.0
S[2, 2] = 1.0
v[2] = 1.0
kcat[2] = 0.1
dG0[2] = -20
KMM[1, 2] = 1e-3
KMM[2, 2] = 1e-1

# add a negative allosteric feedback from X1 to reaction 1
A_inh[1, 1] = 2
K_inh[1, 1] = 2e-5

# add a positive allosteric feedback from X1 to reaction 2
A_act[1, 2] = 2
K_act[1, 2] = 1e-3


lnC_bounds = np.array([[1e-4] * Nc, [1e-4] * Nc], ndmin=2).T
lnC_bounds[1, 0] = 1e-6
lnC_bounds[1, 1] = 1e-2

ecf = EnzymeCostFunction(
    S,
    v,
    kcat,
    dG0,
    KMM=KMM,
    lnC_bounds=lnC_bounds,
    A_act=A_act,
    A_inh=A_inh,
    K_act=K_act,
    K_inh=K_inh,
)


######################################################################

lnC = np.log(np.tile(np.array([1e-4] * Nc, ndmin=2).T, (1, 100)))
lnC[1, :] = np.linspace(-6, -2, 100) * np.log(10)

E0 = np.array([0.5, 0.10, 0.40], ndmin=2).T  # [g]
E1 = np.array([0.5, 0.25, 0.25], ndmin=2).T  # [g]

E = np.hstack([E0, E1])

figs = []
for i in range(E.shape[1]):
    v = ecf.GetFluxes(lnC, E[:, i : i + 1])
    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(1, 2, 1, xscale="log", yscale="log")
    plt.plot(np.exp(lnC[1, :]).flat, v[0, :].flat, "r-", label=r"$v_0$")
    plt.plot(np.exp(lnC[1, :]).flat, v[1, :].flat, "g--", label=r"$v_1$")
    plt.plot(np.exp(lnC[1, :]).flat, v[2, :].flat, "b--", label=r"$v_2$")
    plt.plot(
        np.exp(lnC[1, :]).flat,
        (v[1, :] + v[2, :]).flat,
        "c-",
        label=r"$v_1 + v_2$",
    )
    ax.set_xlabel(r"$y_1$ [M]")
    ax.set_ylabel("flux [M/s]")
    ax.set_ylim(1e-5, 1e-1)
    ax.legend(loc="best")

    ax = fig.add_subplot(1, 2, 2, xscale="log", yscale="linear")
    plt.plot(np.exp(lnC[1, :]).flat, (ecf.S[1, :] @ v).flat)
    ax.set_xlabel(r"$y_1$ [M]")
    ax.set_ylabel(r"$\frac{dy_1}{dt}$ [M/s]")
    figs.append(fig)

simu = EnzymeCostSimulator(ecf)

lnC0 = np.log(1e-4) * np.ones((Nc, 1))

alphas = np.linspace(0, 1, 50)
lnC_steady = np.zeros(alphas.shape)
v_steady = np.zeros(alphas.shape)
for i, alpha in enumerate(alphas):
    v_inf, lnC_inf = simu.Simulate(lnC0, E0 * (1.0 - alpha) + E1 * alpha)
    v_steady[i] = v_inf
    lnC_steady[i] = lnC_inf[1]

fig = plt.figure(figsize=(6, 8))
ax = fig.add_subplot(2, 1, 1, xscale="linear", yscale="log")
ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$y_1$ steady state")
ax.plot(alphas, np.exp(lnC_steady), "-r")
ax = fig.add_subplot(2, 1, 2, xscale="linear", yscale="log")
ax.set_xlabel(r"$\alpha$")
ax.set_ylabel(r"$v$ steady state")
ax.plot(alphas, v_steady, "-g")
figs.append(fig)

pp = PdfPages("res/bistable_branch.pdf")
for fig in figs:
    pp.savefig(fig)
pp.close()
