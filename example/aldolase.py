# -*- coding: utf-8 -*-
"""
Created on Fri Feb 20 11:43:14 2015

@author: noore

Description:
    create a toy diamon network model (i.e. two parallel pathways from A to D, via
    intermediates B or C)

"""

import matplotlib.pyplot as plt
import numpy as np

from ecm.cost_function import EnzymeCostFunction


R = 8.31e-3
DEFAULT_TEMP = 298.15  # K
RT = R * DEFAULT_TEMP

Nr = 3
Nc = 4

S = np.zeros((Nc, Nr))

S[0, 0] = -1
S[1, 0] = 1
S[2, 0] = 1
S[1, 1] = -1
S[2, 1] = 1
S[2, 2] = -1
S[3, 2] = 1

v = np.array([1.0, 1.0, 2.0], ndmin=2).T
kcat = np.array([1.0, 1.0, 1.0], ndmin=2).T
dGm_r = np.array([-3.0, -2.0, -3.0], ndmin=2).T
dG0_r = dGm_r - (RT * np.log(1e-3)) * S.T @ np.ones((Nc, 1))
KMM = np.ones(S.shape)
KMM[S < 0] = 9e-2
KMM[S > 0] = 1e-2

lnC_bounds = np.hstack(
    [np.log(1e-9) * np.ones((Nc, 1)), np.log(1e-1) * np.ones((Nc, 1))]
)

A_act = np.zeros(S.shape)
A_inh = np.zeros(S.shape)
K_act = np.ones(S.shape)
K_inh = np.ones(S.shape)

toy_ecf = EnzymeCostFunction(
    S,
    v,
    kcat,
    dG0_r,
    KMM=KMM,
    lnC_bounds=lnC_bounds,
    A_act=A_act,
    A_inh=A_inh,
    K_act=K_act,
    K_inh=K_inh,
)


#%%
kcat_list = np.logspace(-5, 5, 100)
costs_list = []
for kcat in kcat_list:
    toy_ecf.kcat[1] = kcat
    lnC = None
    while lnC is None:
        lnC = toy_ecf.ECM()
    costs = toy_ecf.ECF(lnC)
    costs_list.append(list(costs.flat))

costs_list = np.array(costs_list)

#%%
fig1 = plt.figure(figsize=(5, 5))
ax = fig1.add_subplot(1, 1, 1)
ax.set_xscale("log")
ax.set_yscale("log")
ax.plot(kcat_list, costs_list)
ax.plot(kcat_list, costs_list.sum(1))
ax.legend(["enzyme 0 cost", "enzyme 1 cost", "enzyme 2 cost", "total cost"])
ax.set_xlabel("$k_{cat}$ of enzyme 1")
fig1.savefig("res/aldolase.pdf")
