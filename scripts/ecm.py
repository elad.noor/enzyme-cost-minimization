import argparse
import inspect
import json
import logging
import os

import matplotlib.pyplot as plt
from sbtab import SBtab


def MakeParser():
    parser = argparse.ArgumentParser(
        description=('Run Enzyme Cost Minimization (ECM)'))
    parser.add_argument('config_fname', type=str,
                        help='Configuration filename')
    return parser


parser = MakeParser()
args = parser.parse_args()

from ecm.model import ECMmodel  # noqa isort:skip

SCRIPT_DIR = os.path.dirname(
    os.path.abspath(inspect.getfile(inspect.currentframe()))
)
BASE_DIR = os.path.split(SCRIPT_DIR)[0]

###############################################################################

with open(args.config_fname, 'r') as fp:
    config = json.load(fp)

input_sbtab_fname = \
    os.path.expanduser(config['IO']['model_sbtab_fname'])
validation_sbtab_fname = \
    os.path.expanduser(config['IO'].get('validation_sbtab_fname', ''))
output_prefix = \
    os.path.expanduser(config['IO']['output_prefix'])

ecf_params = config['ECF']

logging.getLogger().setLevel(logging.INFO)

logging.info('Reading SBtab file: ' + input_sbtab_fname)
modeldata_sbtabdoc = SBtab.read_csv(input_sbtab_fname, 'model')

logging.info('Creating an ECM model using the data')

model = ECMmodel.FromSBtab(modeldata_sbtabdoc, ecf_params=ecf_params)

if validation_sbtab_fname:
    validationdata_sbtabdoc = SBtab.read_csv(
        validation_sbtab_fname, "validation"
    )
    model.AddValidationData(validationdata_sbtabdoc)

logging.info('Solving MDF problem')
lnC_MDF = model.MDF()
logging.info('Solving ECM problem')
lnC_ECM = model.ECM(n_iter=5)

if config['IO']['generate_result_sbtab']:
    res_sbtab = model.ToSBtab(lnC_ECM)
    res_sbtab.write(output_prefix + '.tsv')

if config['IO']['generate_result_figures']:
    fig1 = plt.figure(figsize=(14, 5))
    ax_MDF = fig1.add_subplot(1, 2, 1)
    model.PlotEnzymeDemandBreakdown(lnC_MDF, ax_MDF, plot_measured=True)
    ax_MDF.set_title('MDF results')
    ax_ECM = fig1.add_subplot(1, 2, 2, sharey=ax_MDF)
    model.PlotEnzymeDemandBreakdown(lnC_ECM, ax_ECM, plot_measured=True)
    ax_ECM.set_title('ECF results')
    fig1.savefig(output_prefix + 'enzyme_demand.svg')

    if validation_sbtab_fname:
        fig2 = plt.figure(figsize=(6, 6))
        fig2.suptitle('Metabolite Concentrations')
        ax = fig2.add_subplot(1, 1, 1, xscale='log', yscale='log')
        model.ValidateMetaboliteConcentrations(lnC_ECM, ax)
        fig2.savefig(output_prefix + 'metabolite_validation.svg')

        fig3 = plt.figure(figsize=(6, 6))
        fig3.suptitle('Enzyme Concentrations')
        ax = fig3.add_subplot(1, 1, 1, xscale='log', yscale='log')
        model.ValidateEnzymeConcentrations(lnC_ECM, ax)
        fig3.savefig(output_prefix + 'enzyme_validation.svg')

    fig4 = plt.figure(figsize=(5, 5))
    ax = fig4.add_subplot(1, 1, 1)
    vols, labels, colors = model._GetVolumeDataForPlotting(lnC_ECM)
    ax.pie(vols, labels=labels, colors=colors)
    ax.set_title('total weight = %.2g [g/L]' % sum(vols))
    fig4.savefig(output_prefix + 'pie.svg')
