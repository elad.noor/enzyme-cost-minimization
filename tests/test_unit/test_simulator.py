import sys

import numpy as np
import pytest

from ecm.cost_function import EnzymeCostFunction
from ecm.simulator import EnzymeCostSimulator


@pytest.fixture(scope="module")
def bistable_ecf() -> EnzymeCostFunction:
    Nc = 3
    Nr = 3

    S = np.zeros((Nc, Nr))
    v = np.zeros((Nr, 1))  # [umol/min]
    kcat = np.zeros((Nr, 1))  # [umol/min/mg]
    dG0 = np.zeros((Nr, 1))  # [kJ/mol]
    KMM = np.ones((Nc, Nr))  # [M]
    A_act = np.zeros((Nc, Nr))
    K_act = np.ones((Nc, Nr))  # [M]
    A_inh = np.zeros((Nc, Nr))
    K_inh = np.ones((Nc, Nr))  # [M]

    # v0: X0 -> X1
    S[0, 0] = -1.0
    S[1, 0] = 1.0
    v[0] = 2.0
    kcat[0] = 20.0
    dG0[0] = -30
    KMM[0, 0] = 1e-2
    KMM[1, 0] = 1e-4

    # v1: X1 -> X2
    S[1, 1] = -1.0
    S[2, 1] = 1.0
    v[1] = 1.0
    kcat[1] = 8.0
    dG0[1] = -20
    KMM[1, 1] = 1e-4
    KMM[2, 1] = 1e-1

    # v2: X1 -> X2
    S[1, 2] = -1.0
    S[2, 2] = 1.0
    v[2] = 1.0
    kcat[2] = 0.1
    dG0[2] = -20
    KMM[1, 2] = 1e-3
    KMM[2, 2] = 1e-1

    # add a negative allosteric feedback from X1 to reaction 1
    A_inh[1, 1] = 2
    K_inh[1, 1] = 2e-5

    # add a positive allosteric feedback from X1 to reaction 2
    A_act[1, 2] = 2
    K_act[1, 2] = 1e-3

    lnC_bounds = np.array([[1e-4] * Nc, [1e-4] * Nc], ndmin=2).T
    lnC_bounds[1, 0] = 1e-6
    lnC_bounds[1, 1] = 1e-2

    return EnzymeCostFunction(
        S,
        v,
        kcat,
        dG0,
        KMM=KMM,
        lnC_bounds=lnC_bounds,
        A_act=A_act,
        A_inh=A_inh,
        K_act=K_act,
        K_inh=K_inh,
    )


def test_simulate(bistable_ecf):
    Nc, Nr = bistable_ecf.S.shape
    ln_c = np.log(1e-4) * np.ones((Nc, 1))
    E = np.array([0.5, 0.225, 0.325], ndmin=2).T  # [g]

    simu = EnzymeCostSimulator(bistable_ecf)

    v_inf, lnC_inf = simu.Simulate(ln_c, E)

    assert v_inf == pytest.approx(0.0912, rel=0.01)
    assert lnC_inf[0] == pytest.approx(np.log(1e-4), rel=0.01)
    assert lnC_inf[1] == pytest.approx(-2.15, rel=0.01)
    assert lnC_inf[2] == pytest.approx(np.log(1e-4), rel=0.01)
